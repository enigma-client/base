/* src/main.cpp */

// include files for debugging
#include "src/debug/rw_debug.h"

// include files from Qt
#include <QApplication>
#include <QtWidgets>
// include files from project
#include "src/enigmaclient.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QRect screenSize = QApplication::desktop()->screenGeometry();
    //DEBUG->show();
    // Unedit this command to turn on debugging
    //DEBUG->off();
    //DEBUG->setPos(40, screenSize.height() - 260, 450, 200);
    //DEBUG->print("START UP");
    //DEBUG->stop();
    EnigmaClient client;
    client.resize(350,100);
    client.init();
    client.show();
    return app.exec();
}
