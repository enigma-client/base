/* src/enigmaclient.h */

#ifndef ENIGMACLIENT_H
#define ENIGMACLIENT_H

#include <QMainWindow>

class EnkInterface;
class QLabel;

class EnigmaClient : public QMainWindow
{
    Q_OBJECT
public:
    EnigmaClient(QWidget *parent = 0);
    virtual ~EnigmaClient();
    void init();

private:
    QLabel* enkStatus_m;
    EnkInterface* enkInterface_m;
    bool startup_m;

public slots:
    void slotServerConnected();
    void slotServerDisconnected();
    void slotServerError(int socketError);
    void slotEnkDeactivated();
    void slotEnkInserted();
    void slotEnkReactivated();
    void slotEnkRemoved();
    void slotFocusChange(QWidget* oldW, QWidget* newW);
};
#endif // ENIGMACLIENT
