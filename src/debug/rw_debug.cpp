#include "rw_debug.h"
#include <QMessageBox>
#include <QApplication> 
//#include "debug.h"

RW_Debug* RW_Debug::uniqueInstance = NULL;  // debug not started


/* Gets the RW_Debug instance (creates a new one on first call only)
     returns  pointer to RW_Debug class instance
     Allows access to Form Debug using RW_Debug pointer */
RW_Debug* RW_Debug::instance()
{
    if(uniqueInstance==NULL)
    {
        //RW_DateTime now = RW_DateTime::currentDateTime();
        //RW_String nowStr;
//#if QT_VERSION>=0x030000
  //nowStr = now.toString("yyyyMMdd_hhmmss");
//#else
  //nowStr = now.toString();
//#endif
  //RS_String fName = RS_String("%1/debug_%2.log")
  // .arg(RS_SYSTEM->getHomeDir())
  // .arg(nowStr);
  //RW_String fName = RW_String("debug_%1.log")
   //.arg(nowStr);

        uniqueInstance = new RW_Debug();
        //uniqueInstance->stream = fopen(fName.latin1(), "wt");
        //uniqueInstance->stream = stderr;
    }
    return uniqueInstance;
}


/* Deletes the RS_Debug instance. */
void RW_Debug::deleteInstance() 
{
    if (uniqueInstance!=NULL)
    {
        //fclose(uniqueInstance->stream);
        delete uniqueInstance;
    }
}


/* Constructor for RW_Debug Class - loads Output Panel */
RW_Debug::RW_Debug() 
{
   panel = new Debug();
	on_m = true;
	//runX_m = false;
	aborted_m = false;

   //debugLevel = D_DEBUGGING;
}

/**
 * Sets the debugging level.
 */
//void RS_Debug::setLevel(RS_DebugLevel level) {
    //debugLevel = level;
    //print("RS_DEBUG: Warnings", D_WARNING);
    //print("RS_DEBUG: Errors", D_ERROR);
    //print("RS_DEBUG: Notice", D_NOTICE);
    //print("RS_DEBUG: Informational", D_INFORMATIONAL);
   // print("RS_DEBUG: Debugging", D_DEBUGGING);
//}


/**
 * Gets the current debugging level.
 */
//RS_Debug::RS_DebugLevel RS_Debug::getLevel() {
    //return debugLevel;
//}


void RW_Debug::off()
{
	panel->setVisible(false);
	on_m = false;
}

void RW_Debug::on()
{
	panel->setVisible(true);
	on_m = true;
}

bool RW_Debug::runAborted()
{
	return aborted_m;
}

/*bool RW_Debug::RUNX()
{
	return runX_m;
}*/

/*void RW_Debug::runX(bool state)
{
	runX_m = state;
}*/

void RW_Debug::stop()
{
	if(on_m)
	{
		int ret = QMessageBox::warning(0, "DEBUG BREAK", "BreakPoint", QMessageBox::Ok | QMessageBox::Abort);
		if(ret == QMessageBox::Abort)
		{
			aborted_m = true;
			//QApplication::exit( 0 );
		}
	}
}

void RW_Debug::print(const char* format ...)
{
	if(on_m)
  		panel->print(format);
}

void RW_Debug::print(QString format)
{
	if(on_m)
		panel->print(format);
}

void RW_Debug::printHex(char* string, int length)
{
	int n;
	int column;
	int maxColumns = 8;
	QString hexOutput;
	if(on_m)
	{
		column = 0;
		for (n = 0; n < length; n++)
		{
			column++;
			unsigned char hexNum = (unsigned char)string[n];
			unsigned char hicode = (unsigned char)(hexNum  / 16);
			unsigned char lowcode = hexNum - (hicode * 16);
			if(hicode > 9)
				hicode = hicode + 55;
			else
				hicode = hicode + 48;
			if(lowcode > 9)
				lowcode = lowcode + 55;
			else
				lowcode = lowcode + 48;
			hexOutput.append ("0x" + QString(QChar(hicode)) + QString(QChar(lowcode)) + " ");
			if(column == maxColumns)
			{
				panel->print(hexOutput);
				hexOutput = QString("");
				column = 0;
			}
		}
		if(hexOutput != QString(""))
			panel->print(hexOutput);
	}
}

QString RW_Debug::toHex(char* string, int length)
{
	QString hexOutput;
	int n;
	for (n = 0; n < length; n++)
	{
		unsigned char hexNum = (unsigned char)string[n];
		unsigned char hicode = (unsigned char)(hexNum  / 16);
		unsigned char lowcode = hexNum - (hicode * 16);
		if(hicode > 9)
			hicode = hicode + 55;
		else
			hicode = hicode + 48;
		if(lowcode > 9)
			lowcode = lowcode + 55;
		else
			lowcode = lowcode + 48;
		hexOutput.append ("0x" + QString(QChar(hicode)) + QString(QChar(lowcode)) + " ");
	}
    //if(hexOutput != QString(""))
    return hexOutput;
}

void RW_Debug::show()
{
    panel->show();
}

void RW_Debug::setPos(int x, int y, int w, int h)
{
	panel->setGeometry(x, y, w, h);
}

void RW_Debug::printTimer1Elapsed(QString info)
{
	if(on_m)
		panel->print(info + QString(" elapsed: %1 ms").arg(timer1.msecsTo(QTime::currentTime())));
	/*if(on_m)
	{
		panel->print(info + QString(" elapsed: %1 ms").arg(timer1.msecsTo(QTime::currentTime()) - lastTime_m));
		lastTime_m = timer1.msecsTo(QTime::currentTime());
	}*/
}

int RW_Debug::readTimer1_msecs()
{
	return(timer1.msecsTo(QTime::currentTime()));
}

int RW_Debug::readTimer1_secs()
{
	return(timer1.secsTo(QTime::currentTime()));
}

void RW_Debug::setTimer1()
{
	timer1.restart();
}

void RW_Debug::startTimer1()
{
	timer1.start();
}

//void RW_Debug::stopTimer1()
//{
	//timer1.stop();
//}

void RW_Debug::printTimer2Elapsed(QString info)
{
	if(on_m)
		panel->print(info + QString(" elapsed: %1 ms").arg(timer2.msecsTo(QTime::currentTime())));
}

int RW_Debug::readTimer2_msecs()
{
	return(timer2.msecsTo(QTime::currentTime()));
}

int RW_Debug::readTimer2_secs()
{
	return(timer2.secsTo(QTime::currentTime()));
}

void RW_Debug::setTimer2()
{
	timer2.restart();
}

void RW_Debug::startTimer2()
{
	timer2.start();
}

void RW_Debug::printPerformanceTimer1Elapsed(QString info)
{
	LARGE_INTEGER current;

	if(on_m)
	{
		QueryPerformanceCounter(&current);
		int elapsed = (int)(((double)(current.QuadPart - performanceTimer1_m.inter.QuadPart) / (double)performanceFrequency_m.QuadPart) * 1000);
		performanceTimer1_m.inter.QuadPart = current.QuadPart;
		panel->print(info + QString(" elapsed: %1 ms").arg(elapsed));
	}
}

void RW_Debug::printPerformanceTimer1_secs(QString info)
{
	LARGE_INTEGER current;

	if(on_m)
	{
		QueryPerformanceCounter(&current);
		int elapsed = (int)(((double)(current.QuadPart - performanceTimer1_m.start.QuadPart) / (double)performanceFrequency_m.QuadPart));
		panel->print(info + QString(" elapsed: %1 secs").arg(elapsed));
	}
}

void RW_Debug::printPerformanceTimer1_msecs(QString info)
{
	LARGE_INTEGER current;

	if(on_m)
	{
		QueryPerformanceCounter(&current);
		int elapsed = (int)(((double)(current.QuadPart - performanceTimer1_m.start.QuadPart) / (double)performanceFrequency_m.QuadPart) * 1000);
		panel->print(info + QString(" elapsed: %1 ms").arg(elapsed));
	}
}

int RW_Debug::readPerformanceTimer1_msecs()
{
	LARGE_INTEGER current;

	QueryPerformanceCounter(&current);
	return (int)(((double)(current.QuadPart - performanceTimer1_m.start.QuadPart) / (double)performanceFrequency_m.QuadPart) * 1000);
}

void RW_Debug::startPerformanceTimer1()
{
	performanceTimer1_m.start.QuadPart = 0;
	performanceTimer1_m.stop.QuadPart = 0;
	QueryPerformanceFrequency(&performanceFrequency_m);
	QueryPerformanceCounter(&performanceTimer1_m.start);
	performanceTimer1_m.inter.QuadPart = performanceTimer1_m.start.QuadPart;
}

void RW_Debug::stopPerformanceTimer1()
{
	QueryPerformanceCounter(&performanceTimer1_m.stop);
}

void RW_Debug::printPerformanceTimer2_msecs(QString info)
{
	LARGE_INTEGER current;

	if(on_m)
	{
		QueryPerformanceCounter(&current);
		int elapsed = (int)(((double)(current.QuadPart - performanceTimer2_m.start.QuadPart) / (double)performanceFrequency_m.QuadPart) * 1000);
		panel->print(info + QString(" elapsed: %1 ms").arg(elapsed));
	}
}

int RW_Debug::readPerformanceTimer2_msecs()
{
	LARGE_INTEGER current;

	QueryPerformanceCounter(&current);
	return (int)(((double)(current.QuadPart - performanceTimer2_m.start.QuadPart) / (double)performanceFrequency_m.QuadPart) * 1000);
}

void RW_Debug::startPerformanceTimer2()
{
	performanceTimer2_m.start.QuadPart = 0;
	performanceTimer2_m.stop.QuadPart = 0;
	QueryPerformanceFrequency(&performanceFrequency_m);
	QueryPerformanceCounter(&performanceTimer2_m.start);
	performanceTimer2_m.inter.QuadPart = performanceTimer2_m.start.QuadPart;
}

void RW_Debug::stopPerformanceTimer2()
{
	QueryPerformanceCounter(&performanceTimer2_m.stop);
}

/**
 * Prints the given message to stdout if the current debug level
 * is lower then the given level
 *
 * @param level Debug level.
 */
//void RW_Debug::print(RS_DebugLevel level, const char* format ...){

    //if(debugLevel>=level) {
       // va_list ap;
        //va_start(ap, format);
        //vfprintf(stream, format, ap);
        //fprintf(stream, "\n");
        //va_end(ap);
        //fflush(stream);
    //}

//}


/**
 * Prints a time stamp in the format yyyyMMdd_hhmmss.
 */
//void RS_Debug::timestamp() {
    //RS_DateTime now = RS_DateTime::currentDateTime();
    //RS_String nowStr;

//#if QT_VERSION>=0x030000
  //nowStr = now.toString("yyyyMMdd_hh:mm:ss:zzz ");
//#else
  //nowStr = now.toString();
//#endif
    //fprintf(stream, nowStr.latin1());
    //fprintf(stream, "\n");
    //fflush(stream);
//}


/**
 * Prints the unicode for every character in the given string.
 */
//void RS_Debug::printUnicode(const RS_String& text) {
   //for (int i=0; i<(int)text.length(); i++) {
        //print("[%X] %c", text.at(i).unicode(), text.at(i).latin1());
    //}
//}


// EOF
