/* rw_debugIO.cpp */

#include "src/debug/rw_debugIO.h"

/***************************************************************************************/
/*                                                                                           DebugIO Class                                                                                       */
/***************************************************************************************/
CDebugIO::CDebugIO(QWidget *parent)
      : QTextEdit(parent)
{
     setMinimumSize(700,200);
     setMaximumSize(1000, 1000);
}

void CDebugIO::keyPressEvent(QKeyEvent* event)
{
     //if( event->key() != Qt::Key_Return )
     QTextEdit::keyPressEvent(event);
     if( event->key() == Qt::Key_Shift || event->key() == Qt::Key_Control || event->key() == Qt::Key_Backspace)
          return;
     else
          command(event->key());
    
}

void CDebugIO::command(int key)
{
     QChar ch = QChar(key);
     
     if( newCommand_m == true )
     {
          //debugText_m = txtDebug->text();
          newCommand_m = false;
     }
     if( key == Qt::Key_Return )
     {
          //txtDebug->setText(debugText_m);
          append("Executing command.");
          
          execCommand(commandString_m);
          //append(commandString_m);
          append("");
          moveCursor(QTextCursor::End);
          //txtDebug->append(commandString_m);
          commandString_m = QString("");
          newCommand_m = true;
     }
     else
     {
          commandString_m = commandString_m + ch;
          //txtDebug->setText(debugText_m + commandString_m);
     }
}

void CDebugIO::execCommand(QString command)
{
     /*int com1  = command.indexOf('(');
     QString commandStub = command.left(com1);
     if( commandStub == QString("TODEGREES") )
     {
          int com2 = command.indexOf(')');
          QString data = command.mid(com1 + 1, com2 - com1 - 1);
          double ret = toDegrees(data.toDouble());
          append(command + QString(" = ") + data.setNum(ret) + QString(" degrees"));
     }
     else if( commandStub == QString("TORADIANS") )
     {
          int com2 = command.indexOf(')');
          QString data = command.mid(com1 + 1, com2 - com1 - 1);
          double ret = toRadians(data.toDouble());
          append(command + QString(" = ") + data.setNum(ret) + QString(" radians"));
     }
     else if( commandStub == QString("SLOPETORADIANS") )
     {
          int com2 = command.indexOf(')');
          QString data = command.mid(com1 + 1, com2 - com1 - 1);
          QStringList dataList = data.split(",");
          rwSlope slope = rwSlope();
          slope.gradient = dataList[0].toDouble();
          slope.direction = dataList[1].toInt();
          if( dataList[2] == QString("true") )
               slope.overflow = true;
          else
               slope.overflow = false;
          double ret = slopeToRadians(slope);
          append(command + QString(" = ") + data.setNum(ret) + QString(" radians"));
     }
     else if( commandStub == QString("LINELENGTH") )
     {
          int com2 = command.indexOf(')');
          QString data = command.mid(com1 + 1, com2 - com1 - 1);
          QStringList dataList = data.split(",");
          double ret = lineLength(dataList[0].toDouble(), dataList[1].toDouble(), dataList[2].toDouble(), dataList[3].toDouble());
          append(command + QString(" = ") + data.setNum(ret) + QString(" mm"));
     }
     else if( commandStub == QString("LINESLOPE") )
     {
          QString overflow = QString("");
          int com2 = command.indexOf(')');
          QString data = command.mid(com1 + 1, com2 - com1 - 1);
          QStringList dataList = data.split(",");
          rwSlope ret = lineSlope(dataList[0].toDouble(), dataList[1].toDouble(), dataList[2].toDouble(), dataList[3].toDouble());
          if( ret.overflow == true )
               overflow = QString("true");
          else
               overflow = QString("false");
          append(command + QString(" = ") + data.setNum(ret.gradient) + QString(" : ") + data.setNum(ret.direction) + QString(" : ") + overflow);
     }
     else if( commandStub == QString("LINEANGLE") )
     {
          int com2 = command.indexOf(')');
          QString data = command.mid(com1 + 1, com2 - com1 - 1);
          QStringList dataList = data.split(",");
          double ret = lineAngle(dataList[0].toDouble(), dataList[1].toDouble(), dataList[2].toDouble(), dataList[3].toDouble());
          append(command + QString(" = ") + data.setNum(ret) + QString(" degrees"));
     }
     else if( commandStub == QString("LINEINTERSECT") )
     {
          rwSlope slope1, slope2;
          int com2 = command.indexOf(')');
          QString data = command.mid(com1 + 1, com2 - com1 - 1);
          QStringList dataList = data.split(",");
          slope1.gradient = dataList[2].toDouble();
          slope1.direction = dataList[3].toInt();
         append("dataList[4].data = " + dataList[4]);
          if( dataList[4] == QString("TRUE") )
               slope1.overflow = true;
          else
               slope1.overflow = false;
          slope2.gradient = dataList[7].toInt();
          slope2.direction = dataList[8].toInt();
         append("dataList[9].data = " + dataList[9]);
          if( dataList[9] == QString("TRUE") )
               slope2.overflow = true;
          else
               slope2.overflow = false;

          QPointF* point = lineIntersect(dataList[0].toDouble(), dataList[1].toDouble(), slope1, dataList[5].toDouble(), dataList[6].toDouble(), slope2);
          if( point != 0 )
               append(command + QString(" = point(") + data.setNum(point->x()) + QString(", ") + data.setNum(point->y()) + QString(")."));
     }
     else if( commandStub == QString("ARCORIGIN") )
     {
          int com2 = command.indexOf(')');
          QString data = command.mid(com1 + 1, com2 - com1 - 1);
          QStringList dataList = data.split(",");
          QPointF* point = arcOrigin(dataList[0].toDouble(), dataList[1].toDouble(), dataList[2].toDouble(), dataList[3].toDouble(), dataList[4].toDouble(), dataList[5].toDouble());
          append(command + QString(" = arcOrigin(") + data.setNum(point->x()) + QString(", ") + data.setNum(point->y()) + QString(")."));
     }
     else if( commandStub == QString("SLOPEARCCENTRE") )
     {
          rwSlope lineEntSlope, lineExtSlope;
          int com2 = command.indexOf(')');
          QString data = command.mid(com1 + 1, com2 - com1 - 1);
          QStringList dataList = data.split(",");
          lineEntSlope.gradient = dataList[2].toDouble();
          lineEntSlope.direction = dataList[3].toInt();
          if( dataList[4] == QString("true") )
               lineEntSlope.overflow = true;
          else
               lineEntSlope.overflow = false;
          lineExtSlope.gradient = dataList[5].toInt();
          lineExtSlope.direction = dataList[6].toInt();
          if( dataList[7] == QString("true") )
               lineExtSlope.overflow = true;
          else
               lineExtSlope.overflow = false;
          QPointF* point = arcCentre(dataList[0].toDouble(), dataList[1].toDouble(), lineEntSlope, lineExtSlope, dataList[8].toDouble());
          append(command + QString(" = arcCentre(") + data.setNum(point->x()) + QString(", ") + data.setNum(point->y()) + QString(")."));
     }
     else if( commandStub == QString("POINTARCCENTRE") )
     {
          int com2 = command.indexOf(')');
          QString data = command.mid(com1 + 1, com2 - com1 - 1);
          QStringList dataList = data.split(",");
          QPointF* point = arcCentre(dataList[0].toDouble(), dataList[1].toDouble(), dataList[2].toDouble(), dataList[3].toDouble(), dataList[4].toDouble(), dataList[5].toDouble());
          append(command + QString(" = arcCentre(") + data.setNum(point->x()) + QString(", ") + data.setNum(point->y()) + QString(")."));
     }
     else
     {
          append(QString("Error. No function ") + commandStub);
     }*/
}
