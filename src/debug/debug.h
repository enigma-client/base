/****************************************************************************
** Form interface generated from reading ui file 'prog/util/debug.ui'
**
** Created: Wed Oct 4 07:46:27 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.3   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef DEBUG_H
#define DEBUG_H



#include "rw_debugIO.h"
#include <qwidget.h>

//class QVBoxLayout;
//class QHBoxLayout;
//class QGridLayout;
//class QSpacerItem;

class Debug : public QWidget
{
	Q_OBJECT

public:
	Debug( QWidget* parent = 0, Qt::WindowFlags fl = 0 );
	~Debug();

	void print( const char * message );
	void print( QString message );
	
protected:

protected slots:
	virtual void languageChange();

private:
	CDebugIO* txtDebug;
	void init();

};

#endif // DEBUG_H
