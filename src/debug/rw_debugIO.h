/* rw_debugIO */


#ifndef RW_DEBUGIO_H
#define RW_DEBUGIO_H

#include <QTextEdit>
#include <QKeyEvent>

class CDebugIO : public QTextEdit
{
     Q_OBJECT
public:
     CDebugIO(QWidget *parent = 0);
protected:
     void keyPressEvent(QKeyEvent* event);
private:
     void command(int key);
     void execCommand(QString command);
private:
     bool newCommand_m;
     QString commandString_m;

};

#endif
