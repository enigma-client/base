/* src/enigmaclient.cpp */

// include files from Qt
#include <QtNetwork>
#include <QLabel>
#include <QMessageBox>

// include files from project
#include "inc/enkinterface.h"
#include "src/enigmaclient.h"

#define RELEASE_VERSION "1.0.0"

EnigmaClient::EnigmaClient(QWidget* parent)
{
    // set focus policy to trigger QMainWindow SIGNAL(focusChanged(QWidget*, QWidget* )))
    setFocusPolicy(Qt::StrongFocus);
    // set main window minimum size
    setMinimumSize(480,120);
    // label to display ENK status
    enkStatus_m = new QLabel(this);
    // show label in QMainWindow
    setCentralWidget(enkStatus_m);
    // create new instance of EnkInterface ( supplied in DLL wrapper )
    enkInterface_m = new EnkInterface(this);
    // connect all the signals from the EnkInterface to slot routines ( Qt type of callback function )
    connect(enkInterface_m, SIGNAL(connected()), this, SLOT(slotServerConnected()));
    connect(enkInterface_m, SIGNAL(disconnected()), this, SLOT(slotServerDisconnected()));
    connect(enkInterface_m, SIGNAL(deactivated()), this, SLOT(slotEnkDeactivated()));
    connect(enkInterface_m, SIGNAL(inserted()), this, SLOT(slotEnkInserted()));
    connect(enkInterface_m, SIGNAL(reactivated()), this, SLOT(slotEnkReactivated()));
    connect(enkInterface_m, SIGNAL(removed()), this, SLOT(slotEnkRemoved()));
    connect(enkInterface_m, SIGNAL(connectionError(int)), this, SLOT(slotServerError(int)));
    // connect application focus changed signal to slot routine
    connect(qApp, SIGNAL(focusChanged(QWidget*, QWidget* )), this, SLOT(slotFocusChange(QWidget*, QWidget*)) );
}

EnigmaClient::~EnigmaClient()
{
    // no need to delete child widgets, Qt does it all for us
}

void EnigmaClient::init()
{
    // set startup flag so as to show ENK connection error in slotServerError()
    startup_m = true;
    // initialse ENK Interface and connect to EnigmaServer
    enkInterface_m->initENKServer();
    // following is necessary because signals are not being sent by QApplication at this point
    setWindowTitle("Enigma Client-Base "  + QString(RELEASE_VERSION) + "       No Server Connection");
    // if succesfully connected to EnigmaServer show some information

    /******************************************************************************/
    // following code is user specific and can be removed and replaced with new code
    if(enkInterface_m->isConnected())
    {
        if(enkInterface_m->enkIsInserted())
        {
            int version = enkInterface_m->getVersion();
            setWindowTitle("Enigma Client-Base "  + QString(RELEASE_VERSION) + "       ENK v" + QString("").setNum((float)version / 10));
            if(enkInterface_m->enkIsActive())
            {
                enkStatus_m->setText("        ENK activated.");
            }
            else
            {
                enkStatus_m->setText("        ENK deactivated.");
            }
         }
         else
         {
            setWindowTitle("Enigma Client - Base "  + QString(RELEASE_VERSION) + "       No ENK Connection");
            enkStatus_m->setText("        Please insert and activate your ENK.");
         }
    }
    else
    {
        setWindowTitle("Enigma Client - Base "  + QString(RELEASE_VERSION) + "       No Connection");
        enkStatus_m->setText("        Please restart your enigma server.");
    }
    // end of user specific code
    /******************************************************************************/

    // remove startup flag
    startup_m = false;
}

void EnigmaClient::slotServerConnected()
{
    // triggered by signal connected() from ENKInterface

    /******************************************************************************/
    // following code is user specific and can be removed and replaced with new code

    if(enkInterface_m->enkIsInserted())
    {
        int version = enkInterface_m->getVersion();
        setWindowTitle("Enigma Client-Base "  + QString(RELEASE_VERSION) + "       ENK v" + QString("").setNum((float)version / 10));
        if(enkInterface_m->enkIsActive())
        {
            enkStatus_m->setText("        ENK activated.");
        }
        else
        {
            enkStatus_m->setText("        ENK deactivated.");
        }
     }
     else
     {
        setWindowTitle("Enigma Client - Base "  + QString(RELEASE_VERSION) + "       No ENK Connection");
        enkStatus_m->setText("        Please insert and activate your ENK.");
     }

    // end of user specific code
    /******************************************************************************/

}

void EnigmaClient::slotServerDisconnected()
{
    // triggered by signal disconnected() from ENKInterface

    /******************************************************************************/
    // following code is user specific and can be removed and replaced with new code

    setWindowTitle("Enigma Client - Base "  + QString(RELEASE_VERSION) + "       No Connection");
    enkStatus_m->setText("        Please restart your enigma server.");

    // end of user specific code
    /******************************************************************************/

}

void EnigmaClient::slotServerError(int socketError)
{
    // triggered by signal connectionError() from ENKInterface
    QString connectionError;
    switch (socketError)
    {
        case QLocalSocket::ServerNotFoundError:
            if(startup_m)
                connectionError = (tr("The enigma server was not found.\n\n"
                                                    "Please restart your enigma server."));
            else
                connectionError = QString("");
         break;
        case QLocalSocket::ConnectionRefusedError:
            if(startup_m)
            connectionError = (tr("The connection was refused by the server.\n\n"
                                     "Please make sure that your enigma server is running."));
            else
                connectionError = QString("");
            break;
        case QLocalSocket::PeerClosedError:
         connectionError = (tr("The enigma server closed the connection.\n\n"
                                     "Please restart your enigma server."));
         break;
        default:
         connectionError = (tr("The following error occurred: %1.").arg(socketError));
    }
    if(connectionError != QString(""))
    {
        // important to disconnect focusChanged signal when displaying another QDialog window (QMessageBox)
        disconnect(qApp, SIGNAL(focusChanged(QWidget*, QWidget* )), this, SLOT(slotFocusChange(QWidget*, QWidget*)) );
        QMessageBox::critical(this, "EnigmaClientBase Connection Error", connectionError);
        connect(qApp, SIGNAL(focusChanged(QWidget*, QWidget* )), this, SLOT(slotFocusChange(QWidget*, QWidget*)) );
    }
}

void EnigmaClient::slotEnkDeactivated()
{
    // triggered by signal deactivated() from ENKInterface

    /******************************************************************************/
    // following code is user specific and can be removed and replaced with new code

    enkStatus_m->setText("        Enk deactivated.");

    // end of user specific code
    /******************************************************************************/

}

void EnigmaClient::slotEnkReactivated()
{
    // triggered by signal reactivated() from ENKInterface

    /******************************************************************************/
    // following code is user specific and can be removed and replaced with new code

    enkStatus_m->setText("        Enk activated.");

    // end of user specific code
    /******************************************************************************/

}

void EnigmaClient::slotEnkInserted()
{
    // triggered by signal reactivated() from ENKInterface
    /******************************************************************************/
    // following code is user specific and can be removed and replaced with new code
    int version = enkInterface_m->getVersion();
    setWindowTitle("Enigma Client-Base "  + QString(RELEASE_VERSION) + "       ENK v" + QString("").setNum((float)version / 10));
    enkStatus_m->setText("        Enk deactivated.");

    // end of user specific code
    /******************************************************************************/

}

void EnigmaClient::slotEnkRemoved()
{
    // triggered by signal reactivated() from ENKInterface

    /******************************************************************************/
    // following code is user specific and can be removed and replaced with new code
    setWindowTitle("Enigma Client - Base "  + QString(RELEASE_VERSION) + "       No ENK Connection");
    enkStatus_m->setText("        Please insert and activate your ENK.");

    // end of user specific code
    /******************************************************************************/

}

void EnigmaClient::slotFocusChange(QWidget* oldW, QWidget* newW)
{
    // triggered by signal focusChanged() from QApplication
    // check if application getting focus
    // oldW == 0 then app is receiving focus from outside of app window
    if(oldW == 0)
    {
        enkInterface_m->refresh();
    }
}
