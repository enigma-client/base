
/* src/enkinterface.h */

#ifndef ENKINTERFACE_H
#define ENKINTERFACE_H

#define ENK_REQUEST_IS_INSERTED            9 
#define ENK_REQUEST_IS_ACTIVE             10 

#define ENK_REQUEST_SET_BLOCKSIZE         49
#define ENK_REQUEST_DECRYPT_DATA          55
#define ENK_REQUEST_DECRYPT_FILE          54
#define ENK_REQUEST_ENCRYPT_DATA          51
#define ENK_REQUEST_ENCRYPT_FILE          50 

#define ENK_REQUEST_SIGN_FILE             60
#define ENK_REQUEST_FILE_SIGNATURES       64

#define ENK_REQUEST_SOUND_TAKE_CONTROL    70 
#define ENK_REQUEST_SOUND_RELEASE_CONTROL 71
//#define ENK_REQUEST_SOUND_SET_BLOCKSIZE   72
//#define ENK_REQUEST_SOUND_INIT_ENCODE     73
#define ENK_REQUEST_SOUND_ENCODE          74
//#define ENK_REQUEST_SOUND_INIT_DECODE     75
#define ENK_REQUEST_SOUND_DECODE          76

#define ENK_REQUEST_EHT_DESCRIPTORS       80
#define ENK_REQUEST_CHECK_FILE_EHT        81
#define ENK_REQUEST_GET_EHT_NAME_ID       82
#define ENK_REQUEST_GET_EHT_ID_NAME       83
#define ENK_REQUEST_GET_EHT_ID_INDEX      84

#define ENK_REQUEST_GET_RGN               90
#define ENK_REQUEST_INT_VERSION          100

#define ENK_REQUEST_GET_DECODE_ERROR     200

#if defined BUILD_DLL
 #define BUILD_COMMON_DLLSPEC Q_DECL_EXPORT
#else
 #define BUILD_COMMON_DLLSPEC Q_DECL_IMPORT
#endif

#include <QObject>
#include <QRect>
#include <QTimer>

class QTimer;
class EnkEngine;

class BUILD_COMMON_DLLSPEC EnkInterface : public QObject
{ 
	Q_OBJECT

public:
    EnkInterface(QWidget* parent);
	virtual ~EnkInterface();
	QString checkFileEht(QString ifile);
	QByteArray decryptData(QByteArray data, QPoint pos = QPoint(), bool showProgress = false, int waitTime = 10);
	short decryptFile(QString ifile, QString ofile, QPoint pos = QPoint(), bool showProgress = false, int waitTime = 10);
	//QByteArray encryptData(QByteArray data);
	QByteArray encryptData(QByteArray data, qint16 ehtIndex = 0, QPoint pos = QPoint(), bool showProgress = false, int waitTime = 10); 
	short encryptFile(QString ifile, QString ofile, qint16 ehtIndex = 0, QPoint pos = QPoint(), bool showProgress = false, int waitTime = 10);
	QString getFileSignatures(QString ifile, QString ofile, QPoint pos, bool showProgress = false, int waitTime = 10);
	bool isConnected();
	short enkIsActive();
	short enkIsInserted();
	QStringList getEhtDescriptors();

	int getEhtIdIndex(QString ehtId);
	QString getEhtIdName(QString ehtId);
	QString getEhtNameId(QString ehtName);
	int getEhtNameIndex(QString ehtName);
	int getLastError();
	int getVersion();
	QString getRgn();

	short setEncodedBlockSize(int size);

	QString signFile(QString ifile, QString sinfo, QPoint pos, bool showProgress = false, int waitTime = 10);

	QByteArray soundDecode(QByteArray encryptedData); 
	QByteArray soundEncode(QByteArray data, int blkSize, int ehtIndex);
	short soundReleaseControl();
	short soundTakeControl();

	//short soundInitDecode(QByteArray blockHeader);
	//short soundInitEncode(int ehtIndex);

public slots:
	//void slotShowError(QString error);
	void connectedToServer();
	void disconnectedFromServer();
	void errorConnecting(int error);
	void deactivatedENK();
	void insertedENK();
	void reactivatedENK();
	void removedENK();
	void initENKServer();
	void resetENKServer();
	void refresh();

private slots:


private:
    QWidget* parent_m;
	EnkEngine* engine_m;
	QTimer* pingTimer_m;
	QString currentInput_m;
	quint16 blocksize_m;

	qint16 lastError_m;

signals:
	void connectionError(int error);
	void connected();
	void debug(QString info);
	void disconnected();
	void deactivated();
	void reactivated();
	void inserted(); 
	void removed();
};

#endif // ENKINTERFACE
