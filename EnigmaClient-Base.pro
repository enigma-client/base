#-------------------------------------------------
#
# Project created by QtCreator 2013-09-18T22:30:19
#
#-------------------------------------------------

OBJECTS_DIR = tmp
MOC_DIR = tmp/moc
INCLUDEPATH += . ./inc
win32:CONFIG(release, debug|release): LIBS += -L.\\release\\lib -lenkinterface
else:win32:CONFIG(debug, debug|release): LIBS += -L.\\debug\\lib -lenkinterface

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = EnigmaClient-Base
TEMPLATE = app


SOURCES += src/main.cpp\
           src/enigmaclient.cpp \
    src/debug/debug.cpp \
    src/debug/rw_debug.cpp \
    src/debug/rw_debugIO.cpp

HEADERS  += src/enigmaclient.h \
    src/debug/debug.h \
    src/debug/rw_debug.h \
    src/debug/rw_debugIO.h
